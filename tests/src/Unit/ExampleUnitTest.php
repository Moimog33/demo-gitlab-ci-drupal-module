<?php

namespace Drupal\Tests\my_module\Unit;

use Drupal\my_module\DemoModuleExampleService;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\my_module\DemoModuleExampleService
 *
 * @group my_module
 */
class ExampleUnitTest extends UnitTestCase {

  /**
   * Dummy thing.
   *
   * @var bool
   */
  protected $dummy;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->dummy = new DemoModuleExampleService(TRUE);
  }

  /**
   * @covers ::isDummy
   */
  public function testIsDummy(): void {
    // Dummy test.
    $this->assertEquals($this->dummy->isDummy(), TRUE);
  }

  /**
   * @covers ::isDummy
   */
  // public function testFailDemo() {
  //   $this->assertEquals($this->dummy->isDummy(), FALSE);
  // }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    unset($this->dummy);
  }

}
